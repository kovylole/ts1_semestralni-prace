package org.example;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.NoSuchElementException;

public class Test1 {
    public static void main(String[] args){
        // Path to the Chrome driver
        System.setProperty("webdriver.chrome.driver", "C:\\Java\\TS1\\src\\main\\resources\\chromedriver.exe");

        // Initialize the WebDriver instance
        WebDriver driver = new ChromeDriver();

        // Read data from CSV file
        String csvFile = "C:\\Java\\TS1\\src\\main\\resources\\userInfo.csv";
        String line;
        String csvSplitBy = ",";
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            while ((line = br.readLine()) != null) {
                String[] credentials = line.split(csvSplitBy);
                String username = credentials[0];
                String password = credentials[1];
                // Steam login page
                driver.get("https://store.steampowered.com/login/");

                // Maximize the browser window
                driver.manage().window().maximize();

                // Explicit wait to let the page fully load
                WebDriverWait wait = new WebDriverWait(driver, 10);
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input.newlogindialog_TextInput_2eKVn[type='text']")));

                // Find login and password input elements by CSS selectors
                WebElement usernameInput = driver.findElement(By.cssSelector("input.newlogindialog_TextInput_2eKVn[type='text']"));
                WebElement passwordInput = driver.findElement(By.cssSelector("input.newlogindialog_TextInput_2eKVn[type='password']"));

                // Enter login and password
                usernameInput.sendKeys(username);
                passwordInput.sendKeys(password);

                // Enter login and password
                WebElement loginButton = driver.findElement(By.cssSelector("button[type='submit']"));
                loginButton.click();

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                boolean errorDisplayed = false;
                try {
                    WebElement loginErrorMessage = driver.findElement(By.cssSelector(".newlogindialog_FormError_1Mcy9"));
                    if (loginErrorMessage.isDisplayed()) {
                        System.out.println("Failed to login to user account: " + username);
                        errorDisplayed = true;
                    }
                } catch (NoSuchElementException e) {
                    // Error element not found, continue execution
                }

                if (!errorDisplayed) {
                    System.out.println("Successful login for user: " + username);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Close the browser
        driver.quit();
    }
}
