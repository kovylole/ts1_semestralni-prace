package org.example;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
public class Test3 {
    public static void main(String[] args) {
        // Path to the Chrome driver
        System.setProperty("webdriver.chrome.driver", "C:\\Java\\TS1\\src\\main\\resources\\chromedriver.exe");

        // Initialize the WebDriver instance
        WebDriver driver = new ChromeDriver();

        // Open the Steam Support page
        driver.get("https://help.steampowered.com/en/wizard/HelpWithUnknownCharges");

        // Maximize the browser window
        driver.manage().window().maximize();

        // Find all buttons with the class "help_hide_for_create_request" and select the second button
        WebElement notSteamUserButton = driver.findElements(By.cssSelector("a.help_hide_for_create_request")).get(1);
        notSteamUserButton.click();

        // help info
        String username = "JohnDoe";
        String email = "johndoe@example.com";
        String phoneNumber = "1234567890";
        String chargeAmount = "19.99";
        String message = "I have a problem with a purchase. I would like help in solving this problem.";
        String last4Digits = "1234";

        // Find the email input field and fill it
        WebElement emailInput = driver.findElement(By.cssSelector("input[name='contact_email']"));
        emailInput.sendKeys(email);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Find the name input field and fill it
        WebElement nameInput = driver.findElement(By.cssSelector("input[name='extended_string_billing_name']"));
        nameInput.sendKeys(username);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Find the phone number input field and fill it
        WebElement phoneNumberInput = driver.findElement(By.cssSelector("input[name='extended_string_phone_number']"));
        phoneNumberInput.sendKeys(phoneNumber);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Find the input field for the last 4 digits of the card and fill it in
        WebElement last4DigitsInput = driver.findElement(By.cssSelector("input[name='extended_string_visa_last4']"));
        last4DigitsInput.sendKeys(last4Digits);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Find the input field for the purchase amount and fill it
        WebElement chargeAmountInput = driver.findElement(By.cssSelector("input[name='extended_string_charge_amount']"));
        chargeAmountInput.sendKeys(chargeAmount);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Find the input field for the description of the problem and fill it
        WebElement messageInput = driver.findElement(By.cssSelector("textarea[name='extended_string_message']"));
        messageInput.sendKeys(message);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // The form submit button
        WebElement submitButton = driver.findElement(By.cssSelector("button[type='submit']"));
        submitButton.click();

        // Waiting for a successful send message
        WebElement successMessage = driver.findElement(By.cssSelector(".ticket_sent"));
        System.out.println("Сообщение об успешной отправке: " + successMessage.getText());

        // Close the browser
        driver.quit();
    }
}