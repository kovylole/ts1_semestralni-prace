package org.example;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Test2 {
    public static void main(String[] args) {
        // Path to the Chrome driver
        System.setProperty("webdriver.chrome.driver", "C:\\Java\\TS1\\src\\main\\resources\\chromedriver.exe");

        // Initialize the WebDriver instance
        WebDriver driver = new ChromeDriver();

        // Open the Steam site
        driver.get("https://store.steampowered.com/");

        // Maximize the browser window
        driver.manage().window().maximize();

        // Find the link to the "Categories" tab and click on it
        WebElement categoriesTab = driver.findElement(By.cssSelector("#genre_tab"));
        categoriesTab.click();

        // We are waiting for the window with the choice of categories to appear
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement categoriesWindow = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#genre_flyout")));

        // Find the link to the "Adventure RPG" category and click on it
        WebElement adventureRpgCategoryLink = categoriesWindow.findElement(By.cssSelector("a[href*='adventure_rpg']"));
        adventureRpgCategoryLink.click();

        // Pause to wait for the load loading with the selected category
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Find the link to the first game in the list
        WebElement gameLink = driver.findElement(By.xpath("(//a[contains(@class, 'animated_featured_capsule_Title_3vZJE')])[1]"));

        // Get the text of the name of the game
        String gameTitle = gameLink.getText();
        // Display the name of the game
        System.out.println("Game name: " + gameTitle);

        // Close the browser
        driver.quit();
    }
}
