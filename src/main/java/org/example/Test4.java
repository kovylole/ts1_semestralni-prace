package org.example;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Test4 {
    public static void main(String[] args) throws IOException{
        // Path to the Chrome driver
        System.setProperty("webdriver.chrome.driver", "C:\\Java\\TS1\\src\\main\\resources\\chromedriver.exe");

        // Initialize the WebDriver instance
        WebDriver driver = new ChromeDriver();

        // Read data from CSV file
        String csvFile = "C:\\Java\\TS1\\src\\main\\resources\\itemInfo.csv";
        BufferedReader br = null;
        String line;
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String itemName = line;

                // Open the Steam login page
                driver.get("https://steamcommunity.com/market/");

                // Maximize the browser window
                driver.manage().window().maximize();

                // Search for the element of the search string and enter the name of the game
                WebElement searchInput = driver.findElement(By.id("findItemsSearchBox"));
                searchInput.sendKeys(itemName);
                searchInput.sendKeys(Keys.ENTER);

                // Waiting for search results to load
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // Get the game name of the first element
                WebElement gameNameElement = driver.findElement(By.cssSelector(".market_listing_game_name"));
                String gameName = gameNameElement.getText();

                // Get item price
                WebElement priceElement = driver.findElement(By.cssSelector(".normal_price"));
                String price = priceElement.getText();

                // Output to the console the name of the game and the price of the item
                System.out.println("Game name: " + gameName);
                System.out.println("Item cost: " + itemName + " - " + price);

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                br.close();
            }
        }
        // Close the browser
        driver.quit();
    }
}
