package org.example;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.Keys;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Test5 {
    public static void main(String[] args) throws IOException {
        // Path to the Chrome driver
        System.setProperty("webdriver.chrome.driver", "C:\\Java\\TS1\\src\\main\\resources\\chromedriver.exe");

        // Initialize the WebDriver instance
        WebDriver driver = new ChromeDriver();

        // Read data from CSV file
        String csvFile = "C:\\Java\\TS1\\src\\main\\resources\\gamesName.csv";
        BufferedReader br = null;
        String line;
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                String gameName = line;
                // Open the Steam login page
                driver.get("https://store.steampowered.com/");

                // Maximize the browser window
                driver.manage().window().maximize();

                // Find the search input field and enter the category
                WebElement searchInput = driver.findElement(By.id("store_nav_search_term"));
                searchInput.sendKeys(gameName);

                // Execute pressing the Enter key to perform a search
                Actions actions = new Actions(driver);
                actions.sendKeys(Keys.ENTER).perform();

                // Pause to wait for search results
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // Find the element with the search results (for example, the first game in the list)
                WebElement firstGame = driver.findElement(By.cssSelector(".search_results .search_result_row:nth-child(1)"));
                // Click on the game to open its page
                firstGame.click();
                if (driver.getCurrentUrl().contains("agecheck")) {
                    // Specify your age (here it is assumed that the age is 18+)
                    Select yearDropdown = new Select(driver.findElement(By.id("ageYear")));
                    yearDropdown.selectByValue("2000");

                    // Confirm age and go to product page
                    WebElement ageSubmitButton = driver.findElement(By.cssSelector("a.btnv6_blue_hoverfade.btn_medium"));
                    ageSubmitButton.click();
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                WebElement addToCartButton = driver.findElement(By.cssSelector("a.btn_green_steamui.btn_medium"));
                addToCartButton.click();
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                br.close();
            }
        }
        // Find the "Buy for yourself" button and make a purchase
        WebElement purchaseButton = driver.findElement(By.cssSelector("a.btnv6_green_white_innerfade.btn_medium.continue"));
        purchaseButton.click();

        // Wait for purchase confirmation
        WebElement confirmationMessage = driver.findElement(By.cssSelector("div.checkout_receipt_content h2"));
        System.out.println("Purchase confirmation: " + confirmationMessage.getText());
        // Close the browser
        driver.quit();
    }
}
